# Fsmk Winter Camp 2018

This repo contains all the material for all the tracks in winter camp 2018.

The tracks :-

- Android Development
- Web Development (Ruby on Rails)
- IoT (nodeMCU, Mesh Networks, SDR)


### The different materials will be present in different branches of the main repo. ###

# Downloading Instructions

- Clone the full repo. This will give you access to all the material of all the tracks.
- Clone just the needed branch. (git clone -b branch_name url_of_repo)

# Branch Names.
- [web](https://gitlab.com/fsmk/winterCamp2018/tree/web)
- [android](https://gitlab.com/fsmk/winterCamp2018/tree/android)
- [iot](https://gitlab.com/fsmk/winterCamp2018/tree/iot)
